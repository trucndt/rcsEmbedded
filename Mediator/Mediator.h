/*
 * Mediator.h
 *
 *  Created on: Feb 22, 2017
 *      Author: trucndt
 */

#ifndef MEDIATOR_MEDIATOR_H_
#define MEDIATOR_MEDIATOR_H_

#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <boost/thread.hpp>
#include <boost/program_options.hpp>

#include "Employee.h"
#include "SpokesmanPi1.h"
#include "utils.h"
#include "ServoController.h"

extern boost::program_options::variables_map gPROG_ARGUMENT;

class Employee;
class SpokesmanPi1;

class Mediator
{
public:
	Mediator();

	void start();

    /**
     * @brief comparePassword
     * @param aPass
     * @return true if match
     */
    bool comparePassword(const std::string &aPass);

	virtual ~Mediator();

private:
	uint16_t mServPort;
    std::string mPassword;

    SpokesmanPi1* mSpokesman;
    ServoController* mServoController;

	const static unsigned int MAXPENDING = 5;    /* Maximum outstanding connection requests */
};

#endif /* MEDIATOR_MEDIATOR_H_ */
