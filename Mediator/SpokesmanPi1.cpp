#include "SpokesmanPi1.h"
#include <string.h>

using namespace std;

SpokesmanPi1::SpokesmanPi1(Mediator* aMediator)
{
    mMediator = aMediator;
}

int SpokesmanPi1::initialize()
{
    /* Initializes UART ch2 and create Communication class instance */
    if (Port_initialize(UART_PORT) != COM_NO_ERROR)
    {
        printf("Communication_initialize error.\n");
        return -1;
    }
    Port_set_error_detection(UART_PORT, 1);

    return 0;
}

int SpokesmanPi1::send(const char *aMsg, char *aResponse)
{
    //TODO: Add mutex to protect this block of code

    cout << "Sending to Pi2 ..." << endl;

    int ret = Port_write(UART_PORT, aMsg, strlen(aMsg));
    printf("Port_write return code: %d\n", ret);

    if (ret < 0)
    {
        return ret;
    }

    ret = Port_read(UART_PORT, aResponse, MAX_SIZE_READ);
    printf("Port_write return code: %d\n", ret);

    return ret;
}
