/*
 * Mediator.cpp
 *
 *  Created on: Feb 22, 2017
 *      Author: trucndt
 */

#include "Mediator.h"

using namespace std;

Mediator::Mediator()
{
	mServPort = gPROG_ARGUMENT["port"].as<int>();
    mSpokesman = new SpokesmanPi1(this);
    mServoController = new ServoController();

    //Temporary
    mPassword = "pass1234";
}

Mediator::~Mediator()
{
	// TODO Auto-generated destructor stub
}

void Mediator::start()
{
	int servSock;					/* Socket descriptor for server */
	int clntSock;					/* Socket descriptor for client */
	struct sockaddr_in servAddr; 	/* Local address */
	struct sockaddr_in clntAddr; 	/* Client address */
	unsigned int clntLen;			/* Length of client address data structure */

    /* Initialze Spokesman to hanlde UART */
    mSpokesman->initialize();

    /* Initialize ServoController */
    mServoController->initialize(0);

	/* Create socket for incoming connections */
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		dieWithError("socket() failed\n");
	}

    const int enable = 1;
    if (setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    {
        dieWithError("setsockopt(SO_REUSEADDR) failed");
    }

	/* Construct local address structure */
	memset(&servAddr, 0, sizeof(servAddr));		/* Zero out structure */
	servAddr.sin_family = AF_INET;				/* Internet address family */
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	servAddr.sin_port = htons(mServPort);	  	/* Local port */

	/* Bind to the local address */
	if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
	{
		dieWithError("Aggregator bind() failed");
	}

	/* Mark the socket so it will listen for incoming connections */
	if (listen(servSock, MAXPENDING) < 0)
	{
		dieWithError("listen() failed");
	}

	while (1)
	{
		/* Set the size of the in-out parameter */
		clntLen = sizeof(clntAddr);

		/* Wait for a client to connect */
		cout << "Waiting for client ..." << endl;
		if ((clntSock = accept(servSock, (struct sockaddr *) &clntAddr,
			&clntLen)) < 0)
		{
			cout << "accept() failed" <<endl;
			continue;
		}

		/* ClntSock is connected to a client! */
		string ClientAddr = inet_ntoa(clntAddr.sin_addr);
		uint16_t ClientPort = ntohs(clntAddr.sin_port);

		cout << "Handling client " << ClientAddr << endl;

		/* Start new thread */
        Employee *clnt = new Employee(this, mSpokesman, mServoController, clntSock);
		clnt->start();
    }
}

bool Mediator::comparePassword(const string &aPass)
{
    //TODO: need mutex???
    return (aPass == mPassword);
}


