/*
 * CameraController.h
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

class CameraController
{
public:
    CameraController();
    int enableCamera();
    cv::Mat getNewFrame();

    void startCamera();

    unsigned long totalFrames;

private:
    cv::VideoCapture videoCapture;
    cv::Mat newFrame;
    boost::mutex mtxNewFrame;
    boost::condition_variable condNewFrame;

    bool isNewFrame;

    void updateFrame();
};

#endif // CAMERACONTROLLER_H
