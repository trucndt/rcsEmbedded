/*
 * Spokesman.cpp
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#include "SpokesmanPi2.h"
#include <string.h>

using namespace std;

SpokesmanPi2::SpokesmanPi2()
{

}

void SpokesmanPi2::start()
{
    boost::thread tListen(&SpokesmanPi2::listen, this);
}

int SpokesmanPi2::initialize()
{
    /* Initializes UART ch2 and create Communication class instance */
    if (Port_initialize(UART_PORT) != COM_NO_ERROR)
    {
        printf("Communication_initialize error.\n");
        return -1;
    }
    Port_set_error_detection(UART_PORT, 1);

    return 0;
}

int SpokesmanPi2::send(const char *aMsg)
{
    cout << "Sending to Pi1 ..." << endl;

    int ret = Port_write(UART_PORT, aMsg, strlen(aMsg));
    printf("Port_write return code: %d\n", ret);

    return ret;
}

void SpokesmanPi2::listen()
{
    char rcvBuf[MAX_SIZE_READ];
    int rcvLength;

    while (1)
    {
        if ((rcvLength = Port_read(UART_PORT, rcvBuf, MAX_SIZE_READ)) < 0)
        {
            printf("Port_read error code: %d\n", rcvLength);
            usleep(100000);
            continue;
        }

        rcvBuf[rcvLength] = NULL;
        processReceiveMessage(rcvBuf, rcvLength);
    }
}

int SpokesmanPi2::processReceiveMessage(const char *aMsg, int aMsgSize)
{
    cout << aMsg << endl;

    Json::Reader reader;
    Json::Value root;
    reader.parse(aMsg, aMsg + aMsgSize, root, false);

    if (root["action"] == "requestIP")
    {
        Json::Value response;
        response["IP"] = string("rtsp://") + string(getIPAddress("wlan0")) + "/pi.mpeg4";
        response["port"] = "5554";

        Json::FastWriter writer;
        send(writer.write(response).c_str());
    }
}
