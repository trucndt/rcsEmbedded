/*
 * CameraController.cpp
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#include "CameraController.h"

using namespace cv;
using namespace std;

CameraController::CameraController()
{
	isNewFrame = false;
	totalFrames = 0;
}

int CameraController::enableCamera()
{
	videoCapture.open("/dev/video2");
    videoCapture.set(CV_CAP_PROP_FPS, 30);
	//Check if camera is open
	if (!videoCapture.isOpened())
	{
		cout << "Fail to open camera\n";
		return -1;
	}
	cout << "Open camera successfully.\n";
	return 0;
}

void CameraController::updateFrame()
{
	pthread_setname_np(pthread_self(), "update");
	Mat tmpFrame;
	while(1)
	{
		videoCapture >> tmpFrame;

		if (tmpFrame.empty())
		{
			isNewFrame = false;
			continue;
		}

		totalFrames++;
		boost::unique_lock<boost::mutex> lock(mtxNewFrame);
		tmpFrame.copyTo(newFrame);
		isNewFrame = true;
		lock.unlock();
		condNewFrame.notify_all();
	}
}

Mat CameraController::getNewFrame()
{
	boost::unique_lock<boost::mutex> lock(mtxNewFrame);

	while (!isNewFrame)
	{
		condNewFrame.wait(lock);
	}

	isNewFrame = false;

	return newFrame;
}

void CameraController::startCamera()
{
	boost::thread tCam(&CameraController::updateFrame, this);
}
