/*
 * Spokesman.h
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#ifndef SPOKESMAN_H
#define SPOKESMAN_H

#include <iostream>
#include <app/inc/Port.h>
#include <app/inc/Uart.h>
#include <boost/thread.hpp>
#include <boost/program_options.hpp>
#include <Mediator/json/json.h>
#include <Mediator/utils.h>
#include "ImageProcessing.h"
#include "CameraController.h"

extern boost::program_options::variables_map gPROG_ARGUMENT;

class SpokesmanPi2
{
public:
    SpokesmanPi2();

    /**
     * @brief Create a new thread listen()
     */
    void start();

    /**
     * @brief Continuously monitoring UART port for incomming messages
     */
    void listen();

    /**
     * @brief initialize uart port
     * @return -1 if fail
     */
    int initialize();

private:
    static const Bus UART_PORT = COM_BUS_UART_2;
    static const int MAX_SIZE_READ = 1024;

    /**
     * @brief Process the message received from Pi1
     * @param aMsg: The message received
     * @param aMsgSize: The size of the received message
     * @return not defined yet
     */
    int processReceiveMessage(const char* aMsg, int aMsgSize);

    /**
     * @brief send messages to Pi1
     * @param aMsg: Message to be sent
     * @return <0 if error, otherwise return the number of bytes sent
     */
    int send(const char* aMsg);
};

#endif // SPOKESMAN_H
