#!/bin/bash

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
        echo "** Trapped CTRL-C **"
}

echo "#### Installing v4l2loopback module ####"
echo
echo

sudo modprobe v4l2loopback devices=2
sleep 2

echo "#### Duplicating /dev/video0 to /dev/video1 and /dev/video2 ####"
echo
echo

ffmpeg -loglevel fatal -f v4l2 -r 15 -s 640x480 -i /dev/video0 -vcodec copy -f v4l2 -r 15 -s 640x480 /dev/video1  -vcodec copy -f v4l2 -r 15 -s 640x480 /dev/video2 &
pDup=$!
sleep 2

echo "#### Starting ffserver ####"
echo
echo

ffserver -loglevel fatal -f ffserver.conf &
pServer=$!
sleep 1

ffmpeg -loglevel error -r 20 -s 640x480 -i /dev/video1 http://localhost:12345/feed.ffm &

chmod +x VisionExpert
./VisionExpert

killall -9 ffserver
killall -9 ffmpeg

rm /tmp/feed.ffm
