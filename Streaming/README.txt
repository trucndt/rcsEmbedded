Dependencies:
  - v4l2loopback
  - ffmpeg

Make sure these files are in this folder:
  - ffserver.conf
  - start_streaming.sh
  - VisionExpert (executable file)

Start streaming server:
  $ cd Streaming
  $ ./start_streaming.sh

The stream will be available at: rtsp://<ip>:5554/pi.mpeg4
